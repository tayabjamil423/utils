import { concatTags } from "../src/concatTags";

describe("concatTags", () => {
  it("should handle various cases for concatenating tags", () => {
    expect(concatTags("tag1", "tag2")).toBe("tag1:tag2");
    expect(concatTags("tag1", "tag2", "|")).toBe("tag1|tag2");
    expect(concatTags("tag1")).toBe("tag1");
    expect(concatTags(undefined, "tag2")).toBe("tag2");
    expect(concatTags()).toBeUndefined();
    expect(concatTags("", "tag2")).toBe("tag2");
    expect(concatTags("tag1", "")).toBe("tag1");
  });
});
