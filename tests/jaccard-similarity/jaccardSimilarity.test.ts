import { jaccardSimilarity } from "../../src/jaccardSimilarity";

describe("jaccardSimilarity", () => {
  it("should returns 1 for two identical strings", () => {
    const text1 = "The birch canoe slid on the smooth plain.";
    const text2 = "The birch canoe slid on the smooth plain.";
    const result: number = jaccardSimilarity(text1, text2);

    expect(result).toBe(1);
  });

  it("should return similarity between two slightly different strings", () => {
    const text1 = "The birch canoe slid on the smooth plain.";
    const text2 = "The birch canoe slid on the smoth plain.";
    const result: number = jaccardSimilarity(text1, text2);

    expect(result).toBeGreaterThanOrEqual(0.7);
  });

  it("should return 0 for completely different strings", () => {
    const text1 = "The birch canoe slid on the smooth plain.";
    const text2 = "Completely unrelated string with no common tokens";
    const result: number = jaccardSimilarity(text1, text2);

    expect(result).toBe(0);
  });
});
