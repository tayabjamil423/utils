import { wordTokenizer } from "../../src/jaccardSimilarity";

describe("wordTokenizer", () => {
  it("should correctly tokenize text under various conditions", () => {
    expect(wordTokenizer("hello world")).toEqual(["hello", "world"]);
    expect(wordTokenizer("Hello WoRLD")).toEqual(["hello", "world"]);
    expect(wordTokenizer("  hello world  ")).toEqual(["hello", "world"]);
    expect(wordTokenizer("hello    world")).toEqual(["hello", "world"]);
    expect(wordTokenizer("hello, world! how are you?")).toEqual([
      "hello",
      "world",
      "how",
      "are",
      "you",
    ]);
    expect(wordTokenizer("hello 123 world 456")).toEqual(["hello", "world"]);
    expect(wordTokenizer("hello")).toEqual(["hello"]);
    expect(wordTokenizer("")).toEqual([]);
    expect(wordTokenizer("!!!@@@###")).toEqual([]);
  });
});
