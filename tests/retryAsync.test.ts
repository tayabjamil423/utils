import { delayAsync } from "../src/delayAsync";

import {
  retryAsync,
  E_RetryCancelled,
  E_AllRetriesFailed,
} from "../src/retryAsync";

jest.mock("../src/delayAsync", () => ({
  delayAsync: jest.fn(() => Promise.resolve()),
}));

describe("retryAsync", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should retry function until success", async () => {
    const fn: jest.Mock = jest
      .fn()
      .mockRejectedValueOnce(new Error("Error"))
      .mockRejectedValueOnce(new Error("Error"))
      .mockResolvedValueOnce("result");
    const result: string = await retryAsync<string>({ fn, retries: 2 });

    expect(result).toBe("result");
    expect(fn).toHaveBeenCalledTimes(3);
  });

  it("should successfully execute function on first attempt", async () => {
    const fn: jest.Mock = jest.fn().mockResolvedValue("result");
    const result: string = await retryAsync<string>({ fn, retries: 1 });

    expect(result).toBe("result");
    expect(fn).toHaveBeenCalledTimes(1);
  });

  it("should throw AllRetriesFailed error if all retries fail", async () => {
    const fn: jest.Mock = jest.fn().mockRejectedValue(new Error("Error"));
    const retryPromise: Promise<any> = retryAsync({ fn, retries: 2 });

    await expect(retryPromise).rejects.toMatchObject({
      code: E_AllRetriesFailed,
    });
    expect(fn).toHaveBeenCalledTimes(3);
  });

  it("should throw original error if fn fails and no retries", async () => {
    const fn: jest.Mock = jest.fn().mockRejectedValue(new Error("Error"));
    const retryPromise: Promise<any> = retryAsync({ fn });

    await expect(retryPromise).rejects.toThrow("Error");
  });

  it("should not retry by default", async () => {
    const fn: jest.Mock = jest.fn().mockRejectedValue(new Error("Error"));
    const retryPromise: Promise<any> = retryAsync({ fn });

    await expect(retryPromise).rejects.toThrow("Error");
    expect(fn).toHaveBeenCalledTimes(1);
  });

  it("should pass arguments to function", async () => {
    const fn: jest.Mock = jest.fn().mockResolvedValue(undefined);
    const args = [0, true, "arg3"];
    await retryAsync<string>({ fn, retries: 1, args });

    expect(fn).toHaveBeenCalledWith(...args);
  });

  it("should cancel retrying with RetryCancelled error if errorHandler returns continue: false", async () => {
    const fn = jest.fn().mockRejectedValue(new Error("Error"));
    const errorHandler = jest
      .fn()
      .mockReturnValue({ continue: false, reason: "because" });
    const retryPromise = retryAsync({
      fn,
      retries: 1,
      errorHandler,
    });

    await expect(retryPromise).rejects.toMatchObject({
      code: E_RetryCancelled,
      additionalData: expect.objectContaining({
        reason: "because",
      }),
    });
    expect(fn).toHaveBeenCalledTimes(1);
  });

  it("should continue retrying if errorHandler returns continue: true", async () => {
    const fn = jest
      .fn()
      .mockRejectedValueOnce(new Error("Error"))
      .mockRejectedValueOnce(new Error("Error"))
      .mockResolvedValueOnce("result");
    const errorHandler = jest.fn().mockReturnValue({ continue: true });
    const retryPromise = retryAsync({
      fn,
      retries: 2,
      errorHandler,
    });

    await expect(retryPromise).resolves.toBe("result");
    expect(fn).toHaveBeenCalledTimes(3);
    expect(errorHandler).toHaveBeenCalledTimes(2);
  });

  it("should wait for retryDelay between retries", async () => {
    const fn = jest
      .fn()
      .mockRejectedValueOnce(new Error("Error"))
      .mockRejectedValueOnce(new Error("Error"))
      .mockResolvedValueOnce("result");

    const retryDelay = 100;

    await retryAsync({
      fn,
      retries: 2,
      retryDelay,
    });

    expect(delayAsync).toHaveBeenCalledTimes(2);
    expect(delayAsync).toHaveBeenNthCalledWith(1, retryDelay);
    expect(delayAsync).toHaveBeenNthCalledWith(2, retryDelay);
  });
});
