import { sortByNumeric } from "../src/sortByNumeric";

describe("sortByNumeric", () => {
  it("should sort an array of numbers", () => {
    const input = [3, 1, 2];
    const result = sortByNumeric(input);

    const expected = [1, 2, 3];

    expect(result).toEqual(expected);
  });

  it("should sort an array with integer and float values", () => {
    const input = [1.5, 2, 0.5];
    const result = sortByNumeric(input);

    const expected = [0.5, 1.5, 2];

    expect(result).toEqual(expected);
  });

  it("should work with the default keySelector when items are numbers", () => {
    const input = [10, 5];
    const result = sortByNumeric(input);

    const expected = [5, 10];

    expect(result).toEqual(expected);
  });

  it("should sort an array of objects by the key selector", () => {
    const input = [{ value: 5 }, { value: 2 }, { value: 3 }];
    const result = sortByNumeric(input, (item) => item.value);

    const expected = [{ value: 2 }, { value: 3 }, { value: 5 }];

    expect(result).toEqual(expected);
  });

  it("should return an empty array when given an empty array", () => {
    const input = [];
    const result = sortByNumeric(input);

    const expected = [];

    expect(result).toEqual(expected);
  });

  it("should return the same array when given a single element", () => {
    const input = [42];
    const result = sortByNumeric(input);

    const expected = [42];

    expect(result).toEqual(expected);
  });

  it("should return the same array when all elements are identical", () => {
    const input = [7, 7];
    const result = sortByNumeric(input);

    const expected = [7, 7];

    expect(result).toEqual(expected);
  });
});
