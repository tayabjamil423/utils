import { timeoutAsync, E_Timeout } from "../src/timeoutAsync";

describe("timeoutAsync", () => {
  let clearTimeoutSpy: jest.SpyInstance;

  beforeEach(() => {
    jest.useFakeTimers();
    clearTimeoutSpy = jest.spyOn(global, "clearTimeout");
  });

  afterEach(() => {
    clearTimeoutSpy.mockRestore();
    jest.useRealTimers();
  });

  it("should reject with E_Timeout error after timeout", async () => {
    const promise = new Promise<string>((resolve) =>
      setTimeout(() => resolve("result"), 300)
    );
    const timedOut: Promise<string> = timeoutAsync(promise, 200);

    jest.advanceTimersByTime(200);

    await expect(timedOut).rejects.toMatchObject({
      code: E_Timeout,
    });
  });

  it("should resolve promise within timeout", async () => {
    const promise = new Promise<string>((resolve) =>
      setTimeout(() => resolve("result"), 100)
    );
    const timedOut: Promise<string> = timeoutAsync(promise, 200);

    jest.advanceTimersByTime(100);

    await expect(timedOut).resolves.toBe("result");
  });

  it("should reject with original error if promise rejects within timeout", async () => {
    const promise = new Promise<never>((_, reject) =>
      setTimeout(() => reject(new Error("Error")), 100)
    );
    const timedOut: Promise<string> = timeoutAsync(promise, 200);

    jest.advanceTimersByTime(100);

    await expect(timedOut).rejects.toThrow("Error");
  });

  it("should not timeout by default", async () => {
    const promise = Promise.resolve("result");
    const timedOut: Promise<string> = timeoutAsync(promise);

    expect(timedOut).resolves.toBe("result");
  });

  it("should clear timeout when promise resolves", async () => {
    const promise = new Promise<string>((resolve) =>
      setTimeout(() => resolve("result"), 100)
    );
    const timedOut: Promise<string> = timeoutAsync(promise, 200);

    jest.advanceTimersByTime(100);

    await expect(timedOut).resolves.toBe("result");
    expect(clearTimeoutSpy).toHaveBeenCalled();
  });

  it("should clear timeout when promise rejects", async () => {
    const promise = new Promise<string>((_, reject) =>
      setTimeout(() => reject(new Error("Error")), 100)
    );
    const timedOut: Promise<string> = timeoutAsync(promise, 200);

    jest.advanceTimersByTime(100);

    await expect(timedOut).rejects.toThrow("Error");
    expect(clearTimeoutSpy).toHaveBeenCalled();
  });
});
