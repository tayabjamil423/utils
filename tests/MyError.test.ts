import { MyError, ICreateOptions } from "../src/MyError";

describe("MyError", () => {
  it("should create MyError with specified options", () => {
    const originalError = new Error("Original error");
    const errors = [new Error("Error 1"), new Error("Error 2")];
    const additionalData = { key1: "value1", key2: "value2" };

    const options: ICreateOptions = {
      message: "Test message",
      code: "ErrorCode",
      error: originalError,
      errors: errors,
      additionalData: additionalData,
    };

    const myError = new MyError(options);

    expect(myError.message).toBe("Test message");
    expect(myError.code).toBe("ErrorCode");
    expect(myError.error).toBe(originalError);
    expect(myError.errors).toEqual(errors);
    expect(myError.additionalData).toEqual(additionalData);
  });
});
