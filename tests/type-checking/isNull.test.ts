import { isNull } from "../../src/type-checking/isNull";

describe("isNull", () => {
  it("should return true for null and false for other values", () => {
    expect(isNull(null)).toBe(true);

    expect(isNull(42)).toBe(false);
    expect(isNull(true)).toBe(false);
    expect(isNull("")).toBe(false);
    expect(isNull(undefined)).toBe(false);
    expect(isNull({})).toBe(false);
    expect(isNull([])).toBe(false);
    expect(isNull(function () {})).toBe(false);
  });
});
