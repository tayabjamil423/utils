import { isObject } from "../../src/type-checking/isObject";

describe("isObject", () => {
  it("should correctly identify objects and non-objects", () => {
    expect(isObject({})).toBe(true);
    expect(isObject(new Date())).toBe(true);
    expect(isObject(Math)).toBe(true);

    expect(isObject(0)).toBe(false);
    expect(isObject(false)).toBe(false);
    expect(isObject("")).toBe(false);
    expect(isObject(undefined)).toBe(false);
    expect(isObject(null)).toBe(false);
    expect(isObject([])).toBe(false);
    expect(isObject(function () {})).toBe(false);
  });
});
