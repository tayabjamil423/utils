import { isPrimitive } from "../../src/type-checking/isPrimitive";

describe("isPrimitive", () => {
  it("should correctly identify primitive and non-primitive values", () => {
    expect(isPrimitive(true)).toBe(true);
    expect(isPrimitive(42)).toBe(true);
    expect(isPrimitive(BigInt(123))).toBe(true);
    expect(isPrimitive("")).toBe(true);
    expect(isPrimitive(Symbol("symbol"))).toBe(true);
    expect(isPrimitive(undefined)).toBe(true);

    expect(isPrimitive(null)).toBe(false);
    expect(isPrimitive({})).toBe(false);
    expect(isPrimitive(() => {})).toBe(false);
    expect(isPrimitive([])).toBe(false);
  });
});
