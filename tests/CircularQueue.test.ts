import { CircularQueue } from "../src/CircularQueue";

describe("CircularQueue", () => {
  it("should initialize with a specified capacity", () => {
    const capacity = 5;
    const queue = new CircularQueue<number>(capacity);

    expect(queue.getSize()).toBe(0);
    expect(queue.capacity).toBe(capacity);
  });

  it("should initialize with given items", () => {
    const queue = new CircularQueue<number>(5, [1, 2, 3]);

    expect(queue.toArray()).toEqual([1, 2, 3]);
    expect(queue.getSize()).toBe(3);
  });

  it("should enqueue items correctly", () => {
    const queue = new CircularQueue<number>(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);

    expect(queue.toArray()).toEqual([1, 2, 3]);
    expect(queue.getSize()).toBe(3);
  });

  it("should remove the oldest item on enqueue if the queue is full", () => {
    const queue = new CircularQueue<number>(3);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    queue.enqueue(4);

    expect(queue.toArray()).toEqual([2, 3, 4]);
    expect(queue.getSize()).toBe(3);
  });

  it("should dequeue items correctly", () => {
    const queue = new CircularQueue<number>(3, [1, 2, 3]);

    expect(queue.dequeue()).toBe(1);
    expect(queue.toArray()).toEqual([2, 3]);
    expect(queue.getSize()).toBe(2);
  });

  it("should return undefined on dequeue if the queue is empty", () => {
    const queue = new CircularQueue<number>(3);

    expect(queue.dequeue()).toBeUndefined();
  });

  it("should clear all items from the queue", () => {
    const queue = new CircularQueue<number>(3, [1, 2, 3]);
    queue.clear();

    expect(queue.toArray()).toEqual([]);
    expect(queue.getSize()).toBe(0);
  });

  it("should peek at the top item correctly", () => {
    const queue = new CircularQueue<number>(3, [1, 2, 3]);
    expect(queue.peek()).toBe(1);

    queue.dequeue();
    expect(queue.peek()).toBe(2);

    queue.dequeue();
    expect(queue.peek()).toBe(3);

    queue.dequeue();
    expect(queue.peek()).toBeUndefined();
  });

  it("should convert queue items to an array", () => {
    const queue = new CircularQueue<number>(3, [1, 2, 3]);
    expect(queue.toArray()).toEqual([1, 2, 3]);

    queue.dequeue();
    expect(queue.toArray()).toEqual([2, 3]);

    queue.enqueue(4);
    expect(queue.toArray()).toEqual([2, 3, 4]);
  });

  it("should return the current size of the queue", () => {
    const queue = new CircularQueue<number>(3, [1, 2]);
    expect(queue.getSize()).toBe(2);

    queue.enqueue(3);
    expect(queue.getSize()).toBe(3);

    queue.dequeue();
    expect(queue.getSize()).toBe(2);
  });

  it("should check if the queue is empty", () => {
    const queue = new CircularQueue<number>(3);
    expect(queue.isEmpty()).toBe(true);

    queue.enqueue(1);
    expect(queue.isEmpty()).toBe(false);

    queue.dequeue();
    expect(queue.isEmpty()).toBe(true);
  });

  it("should check if the queue is full", () => {
    const queue = new CircularQueue<number>(2);

    queue.enqueue(1);
    queue.enqueue(2);
    expect(queue.isFull()).toBe(true);

    queue.dequeue();
    expect(queue.isFull()).toBe(false);
  });

  it("should return hasItems as true when the queue has items", () => {
    const queue = new CircularQueue<number>(3);
    expect(queue.hasItems()).toBe(false);

    queue.enqueue(1);
    expect(queue.hasItems()).toBe(true);

    queue.dequeue();
    expect(queue.hasItems()).toBe(false);
  });
});
