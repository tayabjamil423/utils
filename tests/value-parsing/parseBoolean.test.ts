import { parseBoolean } from "../../src/data-parsing/parseBoolean";

describe("parseBoolean", () => {
  it("should correctly parse valid strings, handle boolean values, and manage defaults and edge cases", () => {
    expect(parseBoolean("true")).toBe(true);
    expect(parseBoolean("1")).toBe(true);

    expect(parseBoolean("false")).toBe(false);
    expect(parseBoolean("0")).toBe(false);

    expect(parseBoolean(true)).toBe(true);
    expect(parseBoolean(false)).toBe(false);

    expect(parseBoolean("")).toBeUndefined();
    expect(parseBoolean("invalid")).toBeUndefined();
    expect(parseBoolean(null)).toBeUndefined();
    expect(parseBoolean(undefined)).toBeUndefined();
  });
});
