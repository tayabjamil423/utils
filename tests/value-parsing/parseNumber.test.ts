import { parseNumber } from "../../src/data-parsing/parseNumber";

describe("parseNumber", () => {
  it("should correctly parse valid numeric strings, handle numbers directly, manage defaults, and edge cases", () => {
    expect(parseNumber("123.45")).toBe(123.45);
    expect(parseNumber("0")).toBe(0);
    expect(parseNumber("42")).toBe(42);

    expect(parseNumber(123.45)).toBe(123.45);
    expect(parseNumber(0)).toBe(0);
    expect(parseNumber(42)).toBe(42);

    expect(parseNumber("")).toBeUndefined();
    expect(parseNumber("invalid")).toBeUndefined();
    expect(parseNumber(null)).toBeUndefined();
    expect(parseNumber(undefined)).toBeUndefined();

    expect(parseNumber("", 0)).toBe(0);
    expect(parseNumber("", 100)).toBe(100);
    expect(parseNumber("invalid", 100)).toBe(100);
    expect(parseNumber(null, 100)).toBe(100);
    expect(parseNumber(undefined, 100)).toBe(100);
  });
});
