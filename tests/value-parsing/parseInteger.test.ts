import { myParseInteger } from "../../src/data-parsing/myParseInteger";

describe("myParseInteger", () => {
  it("should correctly parse valid integer strings, handle numbers directly, manage defaults, and edge cases", () => {
    expect(myParseInteger("123")).toBe(123);
    expect(myParseInteger("0")).toBe(0);
    expect(myParseInteger("42")).toBe(42);

    expect(myParseInteger(123)).toBe(123);
    expect(myParseInteger(0)).toBe(0);
    expect(myParseInteger(42)).toBe(42);

    expect(myParseInteger("")).toBeUndefined();
    expect(myParseInteger("invalid")).toBeUndefined();
    expect(myParseInteger(null)).toBeUndefined();
    expect(myParseInteger(undefined)).toBeUndefined();

    expect(myParseInteger("", 0)).toBe(0);
    expect(myParseInteger("", 100)).toBe(100);
    expect(myParseInteger("invalid", 100)).toBe(100);
    expect(myParseInteger(null, 100)).toBe(100);
    expect(myParseInteger(undefined, 100)).toBe(100);
  });
});
