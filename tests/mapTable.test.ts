import { mapTable } from "../src/mapTable";

describe("mapTable", () => {
  it("should map table values based on provided value types", () => {
    const table = {
      key1: "123",
      key2: "5",
      key3: "true",
      key4: "hello",
    };

    const mapObject = {
      key1: { valueType: "number" },
      key2: { valueType: "integer" },
      key3: { valueType: "boolean" },
      key4: { valueType: "string" },
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: 123,
      key2: 5,
      key3: true,
      key4: "hello",
    });
  });

  it("should rename properties based on provided names", () => {
    const table = {
      key1: "123",
      key2: "5",
      key3: "true",
      key4: "hello",
    };

    const mapObject = {
      key1: { name: "newKey1", valueType: "number" },
      key2: { name: "newKey2", valueType: "integer" },
      key3: { name: "newKey3", valueType: "boolean" },
      key4: { name: "newKey4", valueType: "string" },
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      newKey1: 123,
      newKey2: 5,
      newKey3: true,
      newKey4: "hello",
    });
  });

  it("should use string parser for invalid value types", () => {
    const table = {
      key1: "123",
      key2: "true",
    };

    const mapObject = {
      key1: { valueType: "invalid" }, // Invalid type
      key2: { valueType: "boolean" },
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: "123", // Treated as string
      key2: true,
    });
  });

  it("should use default values for defined keys when table values are undefined", () => {
    const table = {
      key1: undefined,
      key2: undefined,
    };

    const mapObject = {
      key1: { defaultValue: true },
      key2: { defaultValue: 100 }, // Default value
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: true,
      key2: 100, // Default value
    });
  });

  it("should use default values for missing keys when table is empty", () => {
    const table = {};

    const mapObject = {
      key1: { valueType: "number", defaultValue: 100 }, // Default value
      key2: { valueType: "string", defaultValue: "default" }, // Default value
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: 100,
      key2: "default",
    });
  });

  it("should apply transformations when a transformer function is provided", () => {
    const table = {
      key1: "123",
      key2: "true",
    };

    const mapObject = {
      key1: { valueType: "number", transformer: (value) => value * 2 },
      key2: { valueType: "boolean", transformer: (value) => !value },
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: 246, // Transformed value
      key2: false, // Transformed value
    });
  });

  it("should return an empty object when table is empty", () => {
    const table = {};

    const mapObject = {
      key1: { valueType: "number", defaultValue: 100 },
      key2: { valueType: "string", defaultValue: "default" },
    };

    const result = mapTable(table, mapObject);

    expect(result).toEqual({
      key1: 100,
      key2: "default",
    });
  });

  it("should return an empty object when mapObject is empty", () => {
    const table = {
      key1: "123",
      key2: "true",
    };

    const mapObject = {};

    const result = mapTable(table, mapObject);

    expect(result).toEqual({});
  });

  it("should return an empty object when both table and mapObject are empty", () => {
    const table = {};
    const mapObject = {};

    const result = mapTable(table, mapObject);

    expect(result).toEqual({});
  });
});
