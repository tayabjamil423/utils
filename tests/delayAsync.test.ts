import { delayAsync } from "../src/delayAsync";

describe("delayAsync", () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.useRealTimers();
  });

  it("should resolve after the specified milliseconds", async () => {
    const promise = delayAsync(100);

    jest.advanceTimersByTime(100);

    await expect(promise).resolves.toBeUndefined();
  });

  it("should resolve immediately by default", async () => {
    const promise = delayAsync();

    jest.advanceTimersByTime(0);

    await promise;
  });
});
