import { objectAssign, IObjectAssignOptions } from "../src/objectAssign";

describe("objectAssign", () => {
  describe("Basic Usage", () => {
    it("should assign properties with default options", () => {
      const target = { a: 1 };
      const source = { a: 3, b: "new" };
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: 3,
        b: "new",
      });
    });
  });

  describe("Overwrite Option", () => {
    it("should overwrite target properties if overwrite = true (default)", () => {
      const target = { a: 1, b: 2 };
      const source = { a: 3, b: 4 };
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: 3,
        b: 4,
      });
    });

    it("should not overwrite target properties if overwrite = false", () => {
      const target = { a: 1, b: 2 };
      const source = { a: 3, b: 4 };
      const result = objectAssign(target, source, { overwrite: false });

      expect(result).toEqual({
        a: 1,
        b: 2,
      });
    });

    it("should overwrite nested properties if overwrite = true and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: 2, c: 3 } };
      const result = objectAssign(target, source, {
        overwrite: true,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 2, c: 3 },
      });
    });

    it("should not overwrite nested properties if overwrite = false and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: 2, c: 3 } };
      const result = objectAssign(target, source, {
        overwrite: false,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 1, c: 3 },
      });
    });
  });

  describe("Foreign Option", () => {
    it("should assign foreign properties if foreign = true (default)", () => {
      const target = { a: 1 };
      const source = { b: 2 };
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: 1,
        b: 2,
      });
    });

    it("should not assign foreign properties if foreign = false", () => {
      const target = { a: 1 };
      const source = { b: 2 };
      const result = objectAssign(target, source, { foreign: false });

      expect(result).toEqual({
        a: 1,
      });
    });

    it("should assign nested foreign properties if foreign = true and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: 2 }, c: { d: 3 } };
      const result = objectAssign(target, source, {
        foreign: true,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 2 },
        c: { d: 3 },
      });
    });

    it("should not assign foreign properties if foreign = false and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: 2 }, c: { d: 3 } };
      const result = objectAssign(target, source, {
        foreign: false,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 2 },
      });
    });
  });

  describe("UndefinedProps Option", () => {
    it("should assign undefined properties if undefinedProps = true (default)", () => {
      const target = { a: 1 };
      const source = { a: undefined, b: 2 };
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: undefined,
        b: 2,
      });
    });

    it("should not assign undefined properties if undefinedProps = false", () => {
      const target = { a: 1 };
      const source = { a: undefined, b: 2 };
      const result = objectAssign(target, source, { undefinedProps: false });

      expect(result).toEqual({
        a: 1,
        b: 2,
      });
    });

    it("should assign nested properties with undefined if undefinedProps = true and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: undefined, c: 2 }, d: 3 };
      const result = objectAssign(target, source, {
        undefinedProps: true,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: undefined, c: 2 },
        d: 3,
      });
    });

    it("should not assign nested properties with undefined if undefinedProps = false and recursive = true", () => {
      const target = { a: { b: 1 } };
      const source = { a: { b: undefined, c: 2 }, d: 3 };
      const result = objectAssign(target, source, {
        undefinedProps: false,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 1, c: 2 },
        d: 3,
      });
    });
  });

  describe("Recursive Option", () => {
    it("should assign recursively if recursive = true", () => {
      const target = { a: { b: 1 }, c: { d: 3 } };
      const source = { a: { b: 2, e: 4 }, c: { f: 5 } };
      const result = objectAssign(target, source, { recursive: true });

      expect(result).toEqual({
        a: { b: 2, e: 4 },
        c: { d: 3, f: 5 },
      });
    });

    it("should assign shallowly if recursive = false", () => {
      const target = { a: { b: 1 }, c: { d: 3 } };
      const source = { a: { b: 2, e: 4 }, c: { f: 5 } };
      const result = objectAssign(target, source, { recursive: false });

      expect(result).toEqual({
        a: { b: 2, e: 4 },
        c: { f: 5 },
      });
    });

    it("should assign nested properties recursively if recursive = true", () => {
      const target = { a: { b: { c: 1 } }, d: 2 };
      const source = { a: { b: { c: 3, e: 4 } }, d: 5 };
      const result = objectAssign(target, source, { recursive: true });

      expect(result).toEqual({
        a: { b: { c: 3, e: 4 } },
        d: 5,
      });
    });
  });

  describe("Empty Inputs", () => {
    it("should handle empty target", () => {
      const target = {};
      const source = { a: 1 };
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: 1,
      });
    });

    it("should handle empty source", () => {
      const target = { a: 1 };
      const source = {};
      const result = objectAssign(target, source);

      expect(result).toEqual({
        a: 1,
      });
    });

    it("should handle empty target and source", () => {
      const target = {};
      const source = {};
      const result = objectAssign(target, source);

      expect(result).toEqual({});
    });
  });

  describe("Real World Examples", () => {
    it("should assign defaults to complex options object (overwrite = false, recursive = true)", () => {
      const target = { a: { b: 1 }, c: { d: 4 } };
      const source = { a: { b: 2 }, c: { e: 5 } };
      const result = objectAssign(target, source, {
        overwrite: false,
        recursive: true,
      });

      expect(result).toEqual({
        a: { b: 1 },
        c: { d: 4, e: 5 },
      });
    });

    it("should assign defaults to options object (foreign = false)", () => {
      const target = { a: 1 };
      const source = { b: 2 };
      const result = objectAssign(target, source, { foreign: false });

      expect(result).toEqual({
        a: 1,
      });
    });
  });
});
