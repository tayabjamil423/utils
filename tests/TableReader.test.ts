import { TableReader } from "../src/TableReader";

describe("TableReader", () => {
  it("should handle various scenarios for read methods", () => {
    const table = {
      intKey: "42",
      numberKey: "3.14",
      boolKey: "true",
      stringKey: "hello",
      invalidIntKey: "abc",
      invalidNumberKey: "xyz",
      invalidBoolKey: "notABool",
      emptyKey: "",
      undefinedKey: undefined as any,
    };
    const tableReader = new TableReader(table);

    expect(tableReader.readInteger("intKey")).toBe(42);
    expect(tableReader.readInteger("invalidIntKey")).toBeUndefined();
    expect(tableReader.readInteger("missingKey")).toBeUndefined();

    expect(tableReader.readNumber("numberKey")).toBe(3.14);
    expect(tableReader.readNumber("invalidNumberKey")).toBeUndefined();
    expect(tableReader.readNumber("missingKey")).toBeUndefined();

    expect(tableReader.readBoolean("boolKey")).toBe(true);
    expect(tableReader.readBoolean("invalidBoolKey")).toBeUndefined();
    expect(tableReader.readBoolean("missingKey")).toBeUndefined();

    expect(tableReader.readString("stringKey")).toBe("hello");
    expect(tableReader.readString("emptyKey")).toBe("");
    expect(tableReader.readString("undefinedKey")).toBeUndefined();
    expect(tableReader.readString("missingKey")).toBeUndefined();
  });
});
