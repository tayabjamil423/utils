import { TableReader } from "./TableReader";

export type TParseValue = number | boolean | string | undefined | any;

export type TValueType = string | "number" | "integer" | "boolean" | "string";

export type TValueTransformer = (value: TParseValue) => TParseValue;

export interface IMapItem {
  name?: string;
  valueType?: TValueType;
  defaultValue?: TParseValue;
  transformer?: TValueTransformer;
}

export type TMapObject = Record<string, IMapItem>;

export function mapTable<TTable extends Record<string, TParseValue> = any>(
  table: Record<string, string | undefined>,
  mapObject: TMapObject
): TTable {
  table = Object(table);

  const reader = new TableReader(table);

  const resultObject = {};
  for (let mapKey in mapObject) {
    const mapItem: IMapItem = mapObject[mapKey];

    let value: TParseValue;
    switch (mapItem.valueType) {
      case "number":
        value = reader.readNumber(mapKey);
        break;
      case "integer":
        value = reader.readInteger(mapKey);
        break;
      case "boolean":
        value = reader.readBoolean(mapKey);
        break;
      case "string":
        value = reader.readString(mapKey);
        break;
      default:
        value = reader.readString(mapKey);
        break;
    }

    value = value ?? mapItem.defaultValue;

    value = mapItem.transformer?.(value) ?? value;

    resultObject[mapItem.name ?? mapKey] = value;
  }

  return resultObject as TTable;
}
