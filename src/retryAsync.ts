import { MyError } from "./MyError";
import { delayAsync } from "./delayAsync";

export interface IErrorHandlerResult {
  continue: boolean;
  reason?: string;
}

export type TErrorHandler = (
  error: Error,
  tryNum: number
) => IErrorHandlerResult;

export interface IRetryParams<TResult = any, TArgs extends any[] = any> {
  fn: (...args: TArgs) => Promise<TResult>;
  args?: TArgs;
  retries?: number;
  retryDelay?: number;
  errorHandler?: TErrorHandler;
}

export const E_RetryCancelled = "RetryCancelled";
export const E_AllRetriesFailed = "AllRetriesFailed";

export async function retryAsync<TResult = any, TArgs extends any[] = any[]>(
  params: IRetryParams<TResult, TArgs>
): Promise<TResult> {
  const { retries: retryCount = 0, errorHandler } = params;
  const totalTries = 1 + retryCount;
  const errors: Error[] = [];

  for (let t = 0; t < totalTries; t++) {
    try {
      const args: TArgs = params.args || ([] as unknown as TArgs);
      const result: TResult = await params.fn(...args);
      return result;
    } catch (error) {
      // If retries is undefined or 0, then throw function's original error
      if (!retryCount) {
        throw error;
      }

      // Collect error for aggregation
      errors.push(error as Error);

      // If it's not last retry
      if (t < retryCount) {
        // If error handler is provided, determine if should continue
        if (errorHandler) {
          const result: IErrorHandlerResult = errorHandler(
            error as Error,
            t + 1
          );
          if (!result.continue) {
            // Throw if error handler return false
            throw new MyError({
              message: "Retry cancelled.",
              code: E_RetryCancelled,
              errors,
              additionalData: {
                reason: result.reason,
              },
            });
          }
        }

        await delayAsync(params.retryDelay);
      }
    }
  }

  // Throw if all retries fail
  throw new MyError({
    message: `Failed after ${totalTries} attempts.`,
    code: E_AllRetriesFailed,
    errors,
  });
}
