export function concatTags(
  tag1?: string,
  tag2?: string,
  delimiter: string = ":"
): string | undefined {
  if (!tag1 && !tag2) {
    return undefined;
  }

  if (!tag1) {
    return tag2;
  }

  if (!tag2) {
    return tag1;
  }

  return tag1 + delimiter + tag2;
}
