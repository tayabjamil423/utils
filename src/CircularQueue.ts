class Node<TItem> {
  value: TItem;
  next: Node<TItem> | null = null;

  constructor(value: TItem) {
    this.value = value;
  }
}

export class CircularQueue<TItem> {
  readonly capacity: number;

  private _head: Node<TItem> | null = null;
  private _tail: Node<TItem> | null = null;
  private _size: number = 0;

  constructor(capacity: number, items: TItem[] = []) {
    this.capacity = capacity;
    for (let item of items) {
      this.enqueue(item);
    }
  }

  enqueue(item: TItem): void {
    if (this.isFull()) {
      this.dequeue();
    }

    const newNode = new Node(item);

    if (this.isEmpty()) {
      this._head = newNode;
      this._tail = newNode;
    } else {
      this._tail.next = newNode;
      this._tail = newNode;
    }

    this._size++;
  }

  dequeue(): TItem | undefined {
    if (this.isEmpty()) {
      return undefined;
    }

    const removedValue = this._head!.value;

    if (this._head === this._tail) {
      this._head = this._tail = null;
    } else {
      this._head = this._head!.next;
    }

    this._size--;
    return removedValue;
  }

  peek(): TItem | undefined {
    if (this.isEmpty()) {
      return undefined;
    }
    return this._head!.value;
  }

  toArray(): TItem[] {
    const result: TItem[] = [];
    let current = this._head;

    while (current !== null) {
      result.push(current.value);
      current = current.next;
    }

    return result;
  }

  clear(): void {
    this._head = this._tail = null;
    this._size = 0;
  }

  isEmpty(): boolean {
    return this._size === 0;
  }

  hasItems(): boolean {
    return !this.isEmpty();
  }

  isFull(): boolean {
    return this._size === this.capacity;
  }

  getSize(): number {
    return this._size;
  }
}
