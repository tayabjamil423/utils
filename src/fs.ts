import fs from "node:fs/promises";

export async function fileExistsAsync(dir: string): Promise<boolean> {
  try {
    await fs.access(dir);
    return true;
  } catch (error) {
    if ((error as NodeJS.ErrnoException).code === "ENOENT") {
      return false;
    } else {
      throw error;
    }
  }
}

export async function recreateDirAsync(path: string): Promise<void> {
  const exists: boolean = await fileExistsAsync(path);
  if (exists) {
    await fs.rm(path, { force: true, recursive: true });
  }
  await fs.mkdir(path, { recursive: true });
}