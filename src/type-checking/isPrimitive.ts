import { isNull } from "./isNull";

export function isPrimitive(value: unknown): boolean {
  // typeof null = "object"
  if (isNull(value)) {
    return false;
  }

  const primitiveTypeNames = [
    "null",
    "undefined",
    "string",
    "number",
    "bigint",
    "boolean",
    "symbol",
  ];
  const valueType: string = typeof value;
  const isPrimitiveTypeName: boolean = primitiveTypeNames.includes(valueType);

  return isPrimitiveTypeName;
}
