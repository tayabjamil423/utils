import { isNull } from "./isNull";

export function isObject(value: unknown): boolean {
  // typeof null = "object"
  if (isNull(value)) {
    return false;
  }

  // typeof array = "object"
  if (Array.isArray(value)) {
    return false;
  }

  if (typeof value !== "object") {
    return false;
  }

  return true;
}
