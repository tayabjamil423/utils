import { isObject } from "./type-checking/isObject";
import { isPrimitive } from "./type-checking/isPrimitive";

export interface IObjectAssignOptions {
  overwrite?: boolean;
  foreign?: boolean;
  undefinedProps?: boolean;
  recursive?: boolean;
}

export function objectAssign<
  TTarget extends Record<string, any>,
  TSource extends Record<string, any>
>(
  targetObj: TTarget,
  sourceObj: TSource,
  options: IObjectAssignOptions = {}
): TTarget & TSource {
  const {
    overwrite = true,
    foreign = true,
    undefinedProps = true,
    recursive = false,
  } = options;

  for (const sourceKey of Object.keys(sourceObj)) {
    let targetValue = targetObj[sourceKey as keyof TTarget];
    let sourceValue = sourceObj[sourceKey];

    // If both values are undefined
    if (targetValue === undefined && sourceValue === undefined) {
      continue;
    }

    // If both values are primitive and equal
    if (
      isPrimitive(targetValue) &&
      isPrimitive(sourceValue) &&
      targetValue === sourceValue
    ) {
      continue;
    }

    // If both values are objects and recursive = true, then assign recursively
    if (isObject(targetValue) && isObject(sourceValue) && recursive) {
      objectAssign(targetValue, sourceValue, options);
      continue;
    }

    // If source value is undefined and assignUndefined = false
    if (sourceValue === undefined && !undefinedProps) {
      continue;
    }

    // If target value is defined and overwrite = false
    if (targetValue !== undefined && !overwrite) {
      continue;
    }

    // If target value is undefined and sourceKey is foreign (to target) and foreign = false
    if (!(sourceKey in targetObj) && !foreign) {
      continue;
    }

    targetObj[sourceKey as keyof TTarget] = sourceValue;
  }

  return targetObj as TTarget & TSource;
}
