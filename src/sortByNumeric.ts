export type TKeySelector<TItem> = (item: TItem) => number;

export function sortByNumeric<TItem>(
  array: TItem[],
  keySelector?: TKeySelector<TItem>
): TItem[] {
  if (!array.length) {
    return [];
  }
  const copy: TItem[] = [...array];
  const sorted = copy.sort((a: TItem, b: TItem) => {
    const valA = keySelector?.(a) || (a as number);
    const valB = keySelector?.(b) || (b as number);
    return valA - valB;
  });
  return sorted;
}
