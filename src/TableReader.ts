import { myParseInteger, parseNumber, parseBoolean } from "./data-parsing";

export class TableReader {
  constructor(readonly table: Record<string, string>) {}

  readNumber(
    key: string,
    defaultValue?: number | undefined
  ): number | undefined {
    return parseNumber(this.table[key], defaultValue);
  }

  readInteger(
    key: string,
    defaultValue?: number | undefined
  ): number | undefined {
    return myParseInteger(this.table[key], defaultValue);
  }

  readBoolean(
    key: string,
    defaultValue?: boolean | undefined
  ): boolean | undefined {
    return parseBoolean(this.table[key], defaultValue);
  }

  readString(
    key: string,
    defaultValue?: string | undefined
  ): string | undefined {
    return this.table[key] ?? defaultValue;
  }
}
