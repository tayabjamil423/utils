import * as Errors from "./MyError";

export const E_Timeout: string = "Timeout";

export function timeoutAsync<TResult>(
  promise: Promise<TResult>,
  timeout?: number
): Promise<TResult> {
  if (timeout === undefined || timeout === 0) {
    return promise;
  }
  return new Promise<TResult>((resolve, reject) => {
    const timer = setTimeout(() => {
      const error = new Errors.MyError({
        message: `Timed out after ${timeout}ms`,
        code: E_Timeout,
      });
      reject(error);
    }, timeout);

    promise
      .then((value) => {
        clearTimeout(timer);
        resolve(value);
      })
      .catch((error) => {
        clearTimeout(timer);
        reject(error);
      });
  });
}
