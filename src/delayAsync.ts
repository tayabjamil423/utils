export function delayAsync(ms: number = 0): Promise<void> {
  if (!ms) {
    return Promise.resolve();
  }
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}
