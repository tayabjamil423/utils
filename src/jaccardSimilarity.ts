export type Tokenizer = (str: string) => string[];

export const wordTokenizer: Tokenizer = (text) => {
  if (!text) {
    return [];
  }

  text = text.trim().toLowerCase();
  const words: string[] = [];
  let currentWord = "";

  for (const char of text) {
    if ((char >= "a" && char <= "z") || (char >= "A" && char <= "Z")) {
      currentWord += char;
    } else {
      if (currentWord) {
        words.push(currentWord);
        currentWord = "";
      }
    }
  }

  if (currentWord) {
    words.push(currentWord);
  }

  return words;
};

export function jaccardSimilarity(
  str1: string,
  str2: string,
  tokenize: Tokenizer = wordTokenizer
): number {
  if (!str1 || !str2) {
    return 0;
  }

  const setA = new Set(tokenize(str1));
  if (setA.size == 0) {
    return 0;
  }

  const setB = new Set(tokenize(str2));
  if (setB.size == 0) {
    return 0;
  }

  const intersection = new Set([...setA].filter((word) => setB.has(word)));
  const union = new Set([...setA, ...setB]);
  const similarity: number = intersection.size / union.size;
  return similarity;
}
