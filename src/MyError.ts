export interface ICreateOptions {
  message?: string;
  code?: string;
  error?: Error;
  errors?: Error[];
  additionalData?: Record<string, any>;
}

export class MyError extends Error {
  code?: string;
  error?: Error;
  errors?: Error[];
  additionalData?: Record<string, any>;

  constructor(options: ICreateOptions) {
    super(options.message);
    delete options.message;
    Object.assign(this, options);
  }
}
