export function parseNumber(
  value?: string | number | null,
  defaultValue: number | undefined = undefined
): number | undefined {
  if (value === undefined || value === null) {
    return defaultValue;
  }

  if (typeof value === "number") {
    return value;
  }

  const parsed: number = parseFloat(value);
  if (isNaN(parsed)) {
    return defaultValue;
  }
  return parsed;
}
