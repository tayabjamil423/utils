export function myParseInteger(
  value?: string | number | null,
  defaultValue: number | undefined = undefined
): number | undefined {
  if (value === undefined || value === null) {
    return defaultValue;
  }

  value = String(value);
  const parsed: number = parseInt(value);
  if (isNaN(parsed)) {
    return defaultValue;
  }
  return parsed;
}
