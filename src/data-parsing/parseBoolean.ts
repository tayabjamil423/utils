export function parseBoolean(
  value?: string | boolean | null,
  defaultValue: undefined | boolean = undefined
): boolean | undefined {
  if (value === undefined || value === null) {
    return defaultValue;
  }

  if (typeof value === "boolean") {
    return value;
  }

  if (value === "0" || value === "false") {
    return false;
  }

  if (value === "1" || value === "true") {
    return true;
  }

  return defaultValue;
}
